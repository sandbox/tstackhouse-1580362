<?php

// $Id: $

/**
 * @file path403.module
 * 
 * Provide path-based 403 error pages to users
 * 
 * @author Tim Stackhouse
 */

/**
 * Implementation oh hook_perm()
 */
function path403_perm() {
  return array(
    'administer path403',
  );
}

/**
 * Implementation of hook_menu()
 */
function path403_menu() {
  return array(
    'path403' => array(
      'access callback' => TRUE,
      'page callback' => 'path403_execute',
    ),
    'admin/settings/path403' => array(
      'title' => 'Path 403 Settings',
      'access callback' => 'user_access',
      'access arguments' => array('administer path403'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('path403_settings_form'),
      'file' => 'path403.admin.inc',
    ),
    'admin/settings/path403/add' => array(
      'title' => 'New Path 403 Message',
      'type' => MENU_CALLBACK,
      'access callback' => 'user_access',
      'access arguments' => array('administer path403'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('path403_message_form'),
      'file' => 'path403.admin.inc',
    ),
    'admin/settings/path403/edit/%' => array(
      'title' => 'Edit Path 403 Message',
      'type' => MENU_CALLBACK,
      'access callback' => 'user_access',
      'access arguments' => array('administer path403'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('path403_message_form', 4),
      'file' => 'path403.admin.inc',
    ),
    'admin/settings/path403/delete/%' => array(
      'title' => 'Delete Path 403 Message',
      'type' => MENU_CALLBACK,
      'access callback' => 'user_access',
      'access arguments' => array('administer path403'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('path403_message_delete_form', 4),
      'file' => 'path403.admin.inc',
    ),
  );
}

/**
 * Page callback for 403 page action
 */
function path403_execute() {
  drupal_add_css(drupal_get_path('module', 'path403') . '/path403.css');
  
  // unless the destination was set, this will be the path that was requested.
  $path = drupal_get_path_alias($_REQUEST['destination']);
  
  $real_path = drupal_lookup_path('source', $path);
  
  $item = menu_get_item($real_path);
  
  // return '<pre>' . print_r($item, TRUE) . '</pre>';
  
  $item['access'] = TRUE;
  
  menu_set_item($real_path, $item);
  
  $query = db_query('SELECT * FROM {path403_messages} ORDER BY weight ASC');
  while ($row = db_fetch_array($query)) {
    $values = unserialize($row['configuration']);
    if (drupal_match_path($path, $values['paths'])) {
      // we found a match, display the appropriate message
      if (user_is_anonymous()) {
        $show_login = variable_get('path403_show_login_form_default', 1);
        $title = 'Access Denied';
        if ($show_login) {
          $title = 'Please Login';
        }

        if (! drupal_get_title()) {
          drupal_set_title($title);
        }
        
        // display anon message and optional login
        $login = '<div class="login-form">' . drupal_get_form('user_login') . '<p class="forgot">' . l(t('Forgot Password?'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.')))) . '</p></div>';
        $login = $values['show_login_form'] ? $login : '';
        return '<div id="path403" class="login">' . $login . '<div class="message">' . $values['anonymous_message'] . '</div></div>';
      }
      else {
        // display authenticated message
        return '<div id="path403" class="nologin"><div class="message">' . $values['authenticated_message'] . '</div></div>';
      }
    }
  }
  
  // if we make it here, no messages matched.  display the appropriate default
  if (user_is_anonymous()) {
    $show_login = variable_get('path403_show_login_form_default', 1);
    $title = 'Access Denied';
    if ($show_login) {
      $title = 'Please Login';
    }
        
    if (! drupal_get_title()) {
      drupal_set_title($title);
    }
    
    // display anon message and optional login
    $login = '<div class="login-form">' . drupal_get_form('user_login') . '<p class="forgot">' . l(t('Forgot Password?'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.')))) . '</p></div>';
    $login =  $show_login ? $login : '';
    return '<div id="path403" class="login">' . $login  . '<div class="message">' . variable_get('path403_anonymous_message_default', 'Please login to continue.') . '</div></div>';
  }
  else {
    drupal_set_title('Access Denied');
    // display authenticated message
    return '<div id="path403" class="nologin"><div class="message">' . variable_get('path403_authenticated_message_default', 'Access Denied.') . '</div></div>';
  }
}


// == END OF FILE: path403.module ======================================================================================