<?php

// $Id: $

/**
 * @file path403.admin.inc
 * 
 * Path 403 Administrate hooks
 * 
 * @author Tim Stackhouse
 */

/**
 * Settings form callback
 */
function path403_settings_form(&$form_state) {
  $form = array();

  $form['new'] = array(
    '#value' => l('Add a new message', 'admin/settings/path403/add'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  
  // print a table of all configured messages
  $messages = array();
  $headers = array('ID', 'Weight', 'Paths', 'Actions');
  
  $query = db_query('SELECT * FROM {path403_messages} ORDER BY weight ASC');
  while ($row = db_fetch_array($query)) {
    $values = unserialize($row['configuration']);
    
    $item = array();
    
    $item[] = $row['mid'];    
    $item[] = isset($values['weight']) ? $values['weight'] : 0;
    $item[] = isset($values['paths']) ? nl2br($values['paths']) : '';
    $item[] = l('edit', 'admin/settings/path403/edit/' . $row['mid']) . ' ' . l('delete', 'admin/settings/path403/delete/' . $row['mid']);
    
    $messages[] = $item;
  }

  // if no messages were added, add a message saying so
  if (empty($messages)) {
    $form['messages_empty'] = array(
      '#value' => '<p>No messages have been configured.</p>',
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }
  else {
    $form['messages'] = array(
      '#value' => theme('table', $headers, $messages) . '<br />',
    );
  }
  
  // add the default message
  $form['message_default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Message'),
    '#description' => t('This message will be used when no other message matches a given path'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer path403'),
  );
  
  // anonymous fieldset
  $form['message_default']['anonymous_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Anonymous User'),
    '#description' => t('These settings will be used when a user is not logged in.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer path403'),
  );
  
  // show login form?
  $form['message_default']['anonymous_user']['path403_show_login_form_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Login Form?'),
    '#default_value' => variable_get('path403_show_login_form_default', 1),
    '#description' => t('Show the login form when this message appears'),
  );
  
  
  // message
  $form['message_default']['anonymous_user']['path403_anonymous_message_default'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('The text of the message to display to anonymous users.'),
    '#default_value' => variable_get('path403_anonymous_message_default', 'Please login to continue.'),
    '#cols' => 60,
    '#rows' => 5,
    '#required' => TRUE,
  );
  
  // authenticated fieldset
  $form['message_default']['authenticated_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authenticated User'),
    '#description' => t('These settings will be used when a user is logged in.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer path403'),
  );
  
  // message
  $form['message_default']['authenticated_user']['path403_authenticated_message_default'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('The text of the message to display to anonymous users.'),
    '#default_value' => variable_get('path403_authenticated_message_default', 'Access Denied.'),
    '#cols' => 60,
    '#rows' => 5,
    '#required' => TRUE,
  );
  
  return system_settings_form($form);
}

/**
 * Message form callback
 */
function path403_message_form(&$form_sate, $mid = NULL) {
  $form = array();
  
  $form['#submit'] = array('path403_save_message');
  
  $message = array();

  if ($mid != NULL && $mid != '') {
    // we were given a message id, load it
    $query = db_query('SELECT * FROM {path403_messages} WHERE mid = %d ORDER BY weight ASC', $mid);
    if (db_affected_rows() != 1) {
      // sanity check if > 1 and make sure it exists
      drupal_not_found();
      return;
    }
    $message = db_fetch_array($query);
    $message['configuration'] = unserialize($message['configuration']);
  }
  
  $form['mid'] = array(
    '#type' => 'value',
    '#value' => isset($message['mid']) ? $message['mid'] : 0,
  );
  
  // evaluation order for this message (show only on overview screen?)
  $form['weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#multiple' => FALSE,
    '#description' => t('Select the order in which this message will be evaluated'),
    '#options' => drupal_map_assoc(range(-50, 50)),
    '#default_value' => isset($message['weight']) ? $message['weight'] : 0,
    '#required' => TRUE,
  );

  // paths to match against
  $form['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#description' => t('Enter paths, one per line, that if matched, will cause this message to be displayed'),
    '#default_value' => isset($message['configuration']['paths']) ? $message['configuration']['paths'] : '',
    '#wysiwyg' => FALSE,
    '#required' => TRUE,
  );
  
  // Anonymous Fieldset
  $form['anonymous_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Anonymous User'),
    '#description' => t('The following settings will be used when anonymous users encounter this message.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer path403'),
  );
  
  
  // show login form?
  $form['anonymous_user']['show_login_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Login Form?'),
    '#default_value' => isset($message['configuration']['show_login_form']) ? $message['configuration']['show_login_form'] : 1,
    '#description' => t('Show the login form when this message appears'),
  );
  
  
  // message
  $form['anonymous_user']['anonymous_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('The text of the message to display to anonymous users.'),
    '#default_value' => isset($message['configuration']['anonymous_message']) ? $message['configuration']['anonymous_message'] : '',
    '#cols' => 60,
    '#rows' => 5,
    '#required' => TRUE,
  );
  
  
  // Authenticated Fieldset
  $form['authenticated_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authenticated User'),
    '#description' => t('The following settings will be used when authenticated users encounter this message.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer path403'),
  );
  
  // message
  $form['authenticated_user']['authenticated_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('The text of the message to display to anonymous users.'),
    '#default_value' => isset($message['configuration']['authenticated_message']) ? $message['configuration']['authenticated_message'] : '',
    '#cols' => 60,
    '#rows' => 5,
    '#required' => TRUE,
  );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save Message'),
  );
  
  $form['delete'] = array(
    '#type' => 'submit',
    '#submit' => array('path403_message_delete_button_handler'),
    '#value' => t('Delete Message'),
  );
  
  
  return $form;
}

/**
 * Submit handler for the message add/edit form
 */
function path403_save_message($form, &$form_state) {
  // construct the message
  $message = new StdClass();
  $message->weight = $form_state['values']['weight'];
  $message->configuration = array(
    'paths'                 => $form_state['values']['paths'],
    'show_login_form'       => $form_state['values']['show_login_form'],
    'anonymous_message'     => $form_state['values']['anonymous_message'],
    'authenticated_message' => $form_state['values']['authenticated_message'],
  );
    
  // new or updated message?
  if ($form_state['values']['mid'] == 0) {
    // new message
    if (drupal_write_record('path403_messages', $message)) {
      // success!
      drupal_set_message('New message saved successfully');
      drupal_goto('admin/settings/path403');
      return;
    }
    else {
      // if we get here, the record failed to write...
      drupal_set_message('There was an error saving the message: ' . db_error(), 'error');
    }
  }
  else {
    // existing message
    $message->mid = $form_state['values']['mid'];
    
    if (drupal_write_record('path403_messages', $message, 'mid')) {
      drupal_set_message('Updated message saved successfully');
      drupal_goto('admin/settings/path403');
      return;
    }
    else {
      // if we get here, the record failed to write...
      drupal_set_message('There was an error updating the message: ' . db_error(), 'error');
    }
  }
}

/**
 * Helper to confirm deleting a message
 */
function path403_message_delete_button_handler($form, &$form_state) {
  if (isset($form_state['values']['mid'])) {
    drupal_goto('admin/settings/path403/delete/' . $form_state['values']['mid']);
  }
  else {
    drupal_not_found();
    return;
  }
}

/**
 * Callback to confirm deleting a message
 * 
 * FIXME: Make me more drupal-ly
 */
function path403_message_delete_form(&$form_sate, $mid = NULL) {
  $message = array();
  if ($mid != NULL && $mid != '') {
    // we were given a message id, load it
    $query = db_query('SELECT * FROM {path403_messages} WHERE mid = %d ORDER BY weight ASC', $mid);
    if (db_affected_rows() != 1) {
      // sanity check if > 1 and make sure it exists
      drupal_not_found();
      return;
    }
    $message = db_fetch_array($query);
    $message['configuration'] = unserialize($message['configuration']);
  }
  else {
    drupal_not_found();
    return;
  }
  
  $form = array('#submit' => array('path403_delete_message'));
  
  $form['confirm'] = array(
    '#value' => 'Are you sure you want to delete this message?',
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );
  
  $form['message'] = array(
    '#value' => wordwrap(print_r($message, TRUE), 200),
    '#prefix' => '<pre>',
    '#suffix' => '</pre>',
  );
  
  $form['mid'] = array(
    '#type' => 'value',
    '#value' => $mid,
  );
  
  
  
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  
  return $form;
}

/**
 * Callback to delete a message
 */
function path403_delete_message($form, &$form_state) {
  if (isset($form_state['values']['mid'])) {
    if (db_query("DELETE FROM {path403_messages} WHERE mid = %d", $form_state['values']['mid'])) {
      drupal_set_message('Message deleted successfully');
      drupal_goto('admin/settings/path403');
    }
    else {
      drupal_set_message('There was an error deleting the message: ' . db_error(), 'error');
    }
  }
  else {
    drupal_set_message('There was an error deleting the message', 'error');
    return;
  }
}

// == END OF FILE: path403.admin.inc ===================================================================================